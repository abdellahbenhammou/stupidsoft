/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package soft;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import gui.MainFrame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;

/**
 *
 * @author Abdellah
 */
public class Soft {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MainFrame mf = new MainFrame();
        
            mf.setVisible(true);
        
    }

    public void OpenAddressBook(JTable mytable, String fileName) {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse(fileName);

            Element root = doc.getDocumentElement();

            NodeList nodelist1 = root.getElementsByTagName("contact");

            String[] st = new String[5];
            System.out.println("the number of contacts: " + nodelist1.getLength());
            for (int i = 0; i < nodelist1.getLength(); i++) {

                Node node = nodelist1.item(i);
//                System.err.println("values: " + node.getChildNodes().item(1).getTextContent()
//                        +" - "+node.getChildNodes().item(2).getTextContent() + " - " + 
//                        node.getChildNodes().item(3).getTextContent());

                st[0] = node.getChildNodes().item(0).getTextContent();
                st[1] = node.getChildNodes().item(1).getTextContent();
                st[2] = node.getChildNodes().item(2).getTextContent();
                st[3] = node.getChildNodes().item(3).getTextContent();
                st[4] = node.getChildNodes().item(4).getTextContent();
                ((DefaultTableModel) mytable.getModel()).addRow(st);
                mytable.setAutoCreateRowSorter(true);
            }
        } catch (Exception ex) {
            System.out.print("error: " + ex.getMessage());
        }
    }

    public void AddContact(String filename, String fullname, String address, String city, String box, String phone)
            throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(filename);
        Element root = document.getDocumentElement();

        // Root Element
        Element rootElement = document.getDocumentElement();

        // contact elements
        Element contact = document.createElement("contact");
        rootElement.appendChild(contact);

        Element nametag = document.createElement("fullname");
        nametag.appendChild(document.createTextNode(fullname));
        contact.appendChild(nametag);

        Element addresstag = document.createElement("address");
        addresstag.appendChild(document.createTextNode(address));
        contact.appendChild(addresstag);

        Element citytag = document.createElement("city");
        citytag.appendChild(document.createTextNode(city));
        contact.appendChild(citytag);

        Element boxtag = document.createElement("pobox");
        boxtag.appendChild(document.createTextNode(box));
        contact.appendChild(boxtag);

        Element phonetag = document.createElement("phone");
        phonetag.appendChild(document.createTextNode(phone));
        contact.appendChild(phonetag);

        root.appendChild(contact);


        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(Soft.class.getName()).log(Level.SEVERE, null, ex);
        }
        StreamResult result = new StreamResult(filename);
        try {
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Logger.getLogger(Soft.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeleteContact(String name, String filename) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            File file = new File(filename);
            Document doc = db.parse(file);

            NodeList contacts = doc.getElementsByTagName("contact");

            if (contacts != null && contacts.getLength() > 0) {
                for (int i = 0; i < contacts.getLength(); i++) {

                    Node node = contacts.item(i);
                    Element e = (Element) node;
                    NodeList nametag = e.getElementsByTagName("fullname");
                    String fullname = nametag.item(0).getChildNodes().item(0).getNodeValue();
                    System.out.println(fullname + " name");

                    if (name.equals(fullname)) {
                        doc.getFirstChild().removeChild(node);
                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transformer = transformerFactory.newTransformer();
                        DOMSource source = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File(filename));
                        transformer.transform(source, result);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void ModifyContact(String[] oldValues,String[] newValues, String filename) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            File file = new File(filename);
            Document doc = db.parse(file);

            NodeList contacts = doc.getElementsByTagName("contact");

            if (contacts != null && contacts.getLength() > 0) {
                for (int i = 0; i < contacts.getLength(); i++) {

                    Node node = contacts.item(i);
                    Element e = (Element) node;
                    NodeList nametag = e.getElementsByTagName("fullname");
                    String fullname = nametag.item(0).getChildNodes().item(0).getNodeValue();
                    NodeList addresstag = e.getElementsByTagName("address");
                    String address = addresstag.item(0).getChildNodes().item(0).getNodeValue();
                    NodeList citytag = e.getElementsByTagName("city");
                    String city = citytag.item(0).getChildNodes().item(0).getNodeValue();
                    NodeList boxtag = e.getElementsByTagName("pobox");
                    String box = boxtag.item(0).getChildNodes().item(0).getNodeValue();
                    NodeList phonetag = e.getElementsByTagName("phone");
                    String phone = phonetag.item(0).getChildNodes().item(0).getNodeValue();
                    System.out.println(fullname + " name");

                    if (oldValues[0].equals(fullname)&&oldValues[1].equals(address)&&
                            oldValues[2].equals(city)&&oldValues[3].equals(box)&&
                            oldValues[4].equals(phone)) {
                        doc.getFirstChild().removeChild(node);
                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transformer = transformerFactory.newTransformer();
                        DOMSource source = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File(filename));
                        transformer.transform(source, result);
                        AddContact(filename, newValues[0], newValues[1], newValues[2], newValues[3], newValues[4]);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public void SaveAs(String filename) {
        try {
            //media.pdf is the basic pdf file that has any initial formating 
            //I make a copy of it to keep it intact for other uses 

            PdfReader pdfReader = new PdfReader("test.pdf");
            //ou hna kandir l copyyy :D
            PdfStamper pdfStamper = null;
            try {
                pdfStamper = new PdfStamper(pdfReader,
                        new FileOutputStream("test1.pdf"));
            } catch (DocumentException ex) {
                Logger.getLogger(Soft.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                PdfContentByte content = pdfStamper.getOverContent(i);
                content.beginText();
                BaseFont bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, "", true);//.createFont();//.createFont(BaseFont.TIMES_ROMAN);
                content.setFontAndSize(bf, 16);

                NodeList nodelist1 = readContacts(filename);
                //notes = "1Notes !!! notes 2Notes !!! notes 3Notes !!! notes 4Notes !!! notes "
                // +"5Notes !!! notes 6Notes !!! notes 7Notes !!! notes !!7Notes !!! notes !!";
                content.showTextAligned(4, "Contacts info:", 250, 750, 0);
                String contact_1 = "", contact_2 = "";
                String[] st = new String[5];
                int x = 700;
                System.out.println("the number of contacts: " + nodelist1.getLength());
                for (int j = 0; j < nodelist1.getLength(); j++) {
                    contact_1 = "";
                    contact_2 = "";
                    Node node = nodelist1.item(j);

                    st[0] = node.getChildNodes().item(0).getTextContent();
                    st[1] = node.getChildNodes().item(1).getTextContent();
                    st[2] = node.getChildNodes().item(2).getTextContent();
                    st[3] = node.getChildNodes().item(3).getTextContent();
                    st[4] = node.getChildNodes().item(4).getTextContent();
                    contact_1 = contact_1 + "- Full name: " + st[0] + " " + "- Address: " + st[1] + " ";
                    contact_2 = contact_2  + "- City: " + st[2] + " " + "- P.O Box: " + st[3] + " " + "- Phone: " + st[4] + "\n";

                    System.out.println("The contact info: " + contact_1);
                    System.out.println("The contact info: " + contact_2);
                    int count = j + 1;
                    content.showTextAligned(0, "Contact " + count, 10, x, 0);
                    x = x-20;
                    
                    content.showTextAligned(0, contact_1, 10, x, 0);
                    x = x-15;
                    
                    content.showTextAligned(0, contact_2, 10, x, 0);
                    x = x-15;
                    
                    content.showTextAligned(0, "------------------------------------------", 10, x, 0);
                    x = x-15;
                }

                content.endText();
            }
            pdfStamper.close();
        } catch (Exception e) {
            System.err.println("here: " + e.getMessage());
        }
    }

    public NodeList readContacts(String fileName) {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        NodeList nodelist1 = null;
        try {
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse(fileName);
            Element root = doc.getDocumentElement();
            nodelist1 = root.getElementsByTagName("contact");
        } catch (Exception ex) {
            System.out.print("error: *" + ex.getMessage());
        }
        return nodelist1;
    }
}